from django.urls import path, include
from . import views
from . views import AboutView

urlpatterns = [
    # path('', views.index),
    path('', AboutView.as_view(), name='weather')
]
