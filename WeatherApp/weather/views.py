import requests
from django.shortcuts import render

from django.views.generic import TemplateView

class AboutView(TemplateView):
    template_name = 'weather/index.html'

def index(request):
    appid = '16fc33439c946a153f6edb465cfc684d'
    url='https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=' + appid

    city= 'London'
    res = requests.get(url.format(city)).json()

    city_info = {
        'city': city,
        'temp': res["main"]["temp"],
        'icon': res["weather"][0]["icon"]
    }
    print(city_info)  
    context = {'info': city_info}

    return render(request, 'weather/index.html', context)
